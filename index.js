const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const cors = require('cors');

app.use(cors());

app.use(express.json());

const port = 8000;

app.post('/encode', async (req, res) => {
   try {
       console.log(req.body);
       const obj = req.body;
       const password = obj.password;
       const message = obj.message;
       const result = Vigenere.Cipher(password).crypt(message);
       res.send(result);
   } catch (e) {
       console.log(e);
   }
});

app.post('/decode', async (req, res) => {
    try {
        const obj = req.body;
        const password = obj.password;
        const message = obj.message;
        const result = Vigenere.Decipher(password).crypt(message);
        res.send(result);
    } catch (e) {
        console.log(e);
    }
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});